import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import Details from './components/Details';
import FlowPage from './components/FlowPage';
import DifferentEvent from './components/DifferentEvent';


class App extends Component<{}> {

  render() {
    return (
      <div>
        <div className="container-fluid">
         <ul className="nav navbar-nav">
           <li><Link to="/">home</Link></li>
           <li><Link to="/eventcheck">EventCheck</Link></li>
           <li><Link to="/flowpage">FlowPage</Link></li>
         </ul>
        </div>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/eventcheck" component={DifferentEvent}/>
          <Route path="/flowpage" component={FlowPage}/>
          <Route path="/:fruitID" component={Details}/>
        </Switch>
      </div>
    );
  }
}

export default App;
