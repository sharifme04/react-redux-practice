import React, { Component } from 'react';
import { connect } from 'react-redux';

type Props={
  title:String,
  body:String,
  fruit:Object,
  id:String
};

class Details extends Component<Props> {

  render() {
    return (
      <div className="App">
       <h3>welcome to {this.props.fruit.title} details page</h3>
        <h4>{this.props.fruit.title}</h4>
        <p>{this.props.fruit.body}</p>
      </div>
    );
  }

}
const mapStateToProps = ( state, ownProps )=>{
  let id = ownProps.match.params.fruitID;
  //let result = state.fruits.filter(fruit => fruit.id === id) //by filter
  return {
    fruit:state.hideShow.fruits.find(fruit=>fruit.id===id)
    //fruit:result[0] // by filter
  }
}

export default connect(mapStateToProps)(Details);
