import React, { Component } from 'react';
import { connect } from 'react-redux';
import {show} from '../actions/EventActions';

type Props={
  hidecomponents: String,
  show:Function
};

class DifferentEvent extends Component<Props> {

  handleClick(value){
    this.props.show(value)
    //console.log(value);
  }

  render() {
    //console.log(hidecomponents);
    return (
      <div>
        { this.props.hidecomponents?<p>click hide to hide me and click show to see me again </p>:null}
        <button type="button" className="btn btn-primary" onClick={()=>this.handleClick(false)}>Hide</button>
        <button type="button" className="btn btn-success"  onClick={()=>this.handleClick(true)}>Show</button>
      </div>
    );
  }

}

const mapStateToProps = (state)=>{
  //console.log(state);
  return {
    //fruits:state.fruits,
    //posts:state.posts
    hidecomponents:state.hideShow.show
  }
}

const mapDispatchToProps = (dispatch:Function)=>{
  //console.log(dispatch);
//  debugger;
  return {
    show: (value)=> {dispatch(show(value))}
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(DifferentEvent);
