import React, { Component } from 'react';
import { connect } from 'react-redux';

class FlowPage extends Component<{}> {

  render() {
    return (
      <div>
       <h3>Age:{this.props.age}</h3> <h3>Slow Age:{this.props.ageSlow}</h3>
       <button className="btn btn-primary" onClick={this.props.increase}>Increase</button>
       <button className="btn btn-info" onClick={this.props.decrease}>Decrease</button>
       <button className="btn btn-warning" onClick={this.props.slowIncrease}>Slow Increase</button>
       <button className="btn btn-danger" onClick={this.props.slowDecrease}>Slow Decrease</button>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    age: state.ageReducer.age,
    ageSlow: state.ageSlow.age
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
  increase : ()=> dispatch({type:'AGE_UP', value:1}),
  decrease : ()=> dispatch({type:'AGE_DOWN', value:1}),
  slowIncrease : ()=> dispatch({type:'AGE_UP_SLOW', value:2}),
  slowDecrease : ()=> dispatch({type:'AGE_DOWN_SLOW', value:2})
}
}

export default connect(mapStateToProps, mapDispatchToProps)(FlowPage);
