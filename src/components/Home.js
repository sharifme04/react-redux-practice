import React, { Component } from 'react';
import { connect } from 'react-redux';
import TableRow from './TableRow';

type Props={
  fruits:Object,
  id:String
};
class Home extends Component<Props> {

componentDidMount() {
  this.timeID = setInterval(
    ()=>this.props.currentTime(),
    1000
  );
}

componentWillUnmount() {
   clearInterval(this.timeID);
}

  render() {
        console.log(this.props.fruits);
        var updatefruits;
        if (this.props.fruits) {
           updatefruits = this.props.fruits.map(fruit=>
             <TableRow key={fruit.id} fruit={fruit}/>
           )
       }
    return (
      <div className="container-fluid">
      <h3>{this.props.nowTime.toLocaleTimeString()}</h3>
        <div className="table-responsive">
         <table className="table table-default">
          <thead>
            <tr>
              <th>ID</th>
              <th>Title</th>
              <th>Body</th>
              <th>Fruits Details</th>
            </tr>
          </thead>
          <tbody>
            {updatefruits}
          </tbody>
        </table>
       </div>
      </div>
    );
  }

}

const mapStateToProps = (state)=>{
  console.log(state);
  return {
    fruits:state.hideShow.fruits,
    //posts:state.posts
    nowTime:state.dateTime.date
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
   currentTime : ()=> dispatch({type:'DATE_TIME_UPDATE', value:new Date()})
 }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
