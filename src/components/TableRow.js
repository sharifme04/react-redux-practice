import React from 'react';
import { Link } from 'react-router-dom';


const TableRow = ({ fruit } : {fruit : Object}) =>(
      <tr>
        <td>{fruit.id}</td>
        <td>{fruit.title}</td>
        <td>{fruit.body}</td>
        <td><Link to={`${fruit.id}`}><button type="button" className="btn btn-info">Details</button></Link></td>
      </tr>
    );
export default TableRow;
