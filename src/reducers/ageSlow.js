
const initialState = {
  age: 20
};

const ageSlow = (state = initialState, action)=> {
  const newState = {...state};

  switch (action.type) {
    case 'AGE_UP_SLOW_Async':
       newState.age += action.value
      break;
    case 'AGE_DOWN_SLOW_Async':
        newState.age -= action.value
      break;
    default:
      return newState
  }
  return newState;

}

export default ageSlow;
