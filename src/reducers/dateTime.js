const initialState = {
  date: new Date()
};

const dateTime = (state = initialState, action)=> {
  const newState = {...state};

  switch (action.type) {
    case 'DATE_TIME_UPDATE':
       newState.date = action.value
      break;
    default:
      return newState
  }
  return newState;

}

export default dateTime;
