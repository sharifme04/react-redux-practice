import fruits from './data.json';

const initialState = {
  posts:[
    {id:'1', title:'post1' , body:'body of the post1 '},
    {id:'2', title:'post2' , body:'body of the post2 '},
    {id:'3', title:'post3' , body:'body of the Post3 '}
  ],
  fruits:fruits,
  show:true
}

const hideShow = ( state:Object = initialState, action:Object) =>{
  console.log(action);
  if (action.type==="SHOW_CONTENT") {
   return {...state, show:action.show}
  }
  return state;
}

export default hideShow;
