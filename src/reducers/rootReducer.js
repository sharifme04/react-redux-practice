import { combineReducers } from 'redux';
import hideShow from './hideShow';
import ageReducer from './ageReducer';
import ageSlow from './ageSlow';
import dateTime from './dateTime'

export default combineReducers({
  hideShow,
  ageReducer,
  dateTime,
  ageSlow
});
