//import { delay } from 'redux-saga';
import {takeEvery ,delay, put} from 'redux-saga/effects';

function* ageUpAsync(){
  yield delay(4000);
  yield put({type:'AGE_UP_SLOW_Async', value:2});
}

function* ageDownAsync(){
  yield delay(4000);
  yield put({type:'AGE_DOWN_SLOW_Async', value:2});
}

export function* ageSlow(){
  yield takeEvery('AGE_UP_SLOW', ageUpAsync);
  yield takeEvery('AGE_DOWN_SLOW', ageDownAsync);
}
